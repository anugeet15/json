package com.sample.jsonparsing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;



class RowAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String, String>> rowList;
    LayoutInflater inflter;
    HashMap<String,String> rowValue;

    public RowAdapter(Context applicationContext, ArrayList<HashMap<String, String>> rowList) {
        this.context = applicationContext;
        this.rowList = rowList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return rowList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.list_item, null);//set layout for displaying items
        rowValue = rowList.get(i);
        TextView title = (TextView) view.findViewById(R.id.text_title);
        TextView desc = (TextView) view.findViewById(R.id.textView2);
        ImageView icon = (ImageView) view.findViewById(R.id.imageView);

        title.setText(rowValue.get("title"));
        desc.setText(rowValue.get("description"));

        Picasso.with(context).load(rowValue.get("imageHref")).into(icon);

        return null;

    }


}
