package com.sample.jsonparsing;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    TextView txtTitle;
    ListView lvRow;
    ArrayList<HashMap<String, String>> rowList;
    RowAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rowList = new ArrayList<>();
        txtTitle = (TextView) findViewById(R.id.text_title);
        lvRow = (ListView) findViewById(R.id.list_row);

        new GetRows().execute();
    }

    private class GetRows extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Json Data is downloading",Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            HttpHandler sh = new HttpHandler();
            //Making a request to url and waiting for response
            String url = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json";
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONObject title = jsonObj.getJSONObject("title");
                    txtTitle.setText(""+title);

                    // Getting JSON Array node
                    JSONArray rows = jsonObj.getJSONArray("rows");

                    // looping through All Rows
                    for (int i = 0; i < rows.length(); i++) {
                        JSONObject c = rows.getJSONObject(i);
                        String rowTitle = c.getString("title");
                        String rowDescription = c.getString("description");
                        String rowImage = c.getString("imageHref");


                        // tmp hash map for single contact
                        HashMap<String, String> row = new HashMap<>();

                        // adding each child node to HashMap key => value
                        row.put("title", rowTitle);
                        row.put("description", rowDescription);
                        row.put("imageHref", rowImage);

                        // adding contact to contact list
                        rowList.add(row);
                    }
                }catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }  else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            adapter = new RowAdapter(getApplicationContext(), rowList);
            lvRow.setAdapter(adapter);
        }
    }
}
